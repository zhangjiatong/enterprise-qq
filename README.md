# Enterprise-QQ

<a href='https://gitee.com/zhangjiatong/enterprise-qq'><img src='https://gitee.com/zhangjiatong/enterprise-qq/widgets/widget_5.svg' alt='Fork me on Gitee'></img></a>

<a href='https://gitee.com/oschina/git-osc/stargazers'><img src='https://gitee.com/oschina/git-osc/badge/star.svg?theme=dark' alt='star'></img></a><a href='https://gitee.com/oschina/git-osc/members'><img src='https://gitee.com/oschina/git-osc/badge/fork.svg?theme=gray' alt='fork'></img></a>

#### 介绍
🚀🚀🚀基于Qt框架开发的企业版QQ 🐧🐧 客户端和服务端。致敬QQ产品设计，支持传输文本、表情包、文件，后续计划扩展一对一通话和群内会议，本工程为基于兴趣爱好的开源工程，不做任何商业用途。

#### 软件架构
1、企业QQ是一个给企业使用的即时通讯软件，该项目采用C/S架构，服务器端主要分为管理员登录、管理员工和管理部门等，客户端主要分为用户注册登录、用户修改个人信息、用户群聊和私聊等。

2、项目通过MySql来进行数据存储，通过QT框架、QSS样式表来实现优美的界面，并将数据展示在界面上。

3、能够实现即时聊天、收发文件等常用功能。

![image-20230307005441649](./Document/Picture/image-20230307005441649.png)

**技术实现**：

1、通过QT框架进行软件界面搭建。

2、通过QSS对界面进行优化。

3、通过MySql进行数据库的创建。

4、利用QT信号与槽机制和SQL语句实现项目基本功能。

5、利用TCP和UDP实现群聊和私聊。

6、使用多线程实现发送文件和接受文件。

**登录界面:**

![Snipaste_2023-03-01_18-39-00](./Document/Picture/Snipaste_2023-03-01_18-39-00.png)


#### 安装教程

1. 下载VS2019

   VS 安装时工作符合选择 使用C++的桌面开发就够了

   ![image-20230307000427096](./Document/Picture/image-20230307000427096.png)

2. Qt 5.14(这个镜像链接下载Qt比较快：[Index of /archive/qt](http://download.qt.io/archive/qt/) ) 

   ![image-20230307001228072](./Document/Picture/image-20230307001228072.png)

   ![image-20230307001300705](./Document/Picture/image-20230307001300705.png)

   勾选自己需要的版本，建议32,64位的都勾上，没有VS2019,  但是vsaddin插件依然可以找到VS2017的Qt, 安装步骤主要截图如下：

   ![image-20230307001403772](./Document/Picture/image-20230307001403772.png)

3. qt-vsaddin-msvc2019(链接：[Index of /archive/vsaddin/2.8.1](https://download.qt.io/archive/vsaddin/2.8.1/))

   ![image-20230307001842412](./Document/Picture/image-20230307001842412.png)

   Qt安装结束后，再安装Qt VS插件

   ![image-20230307001515546](./Document/Picture/image-20230307001515546.png)

    选择VS2019的版本, 我选的是2.5版本

   ![image-20230307001606766](./Document/Picture/image-20230307001606766.png)

   详细内容可以参考这位大佬的博客：[(2条消息) VS2019 Qt开发环境搭建与配置_qt vs2019_令狐掌门的博客-CSDN博客](https://blog.csdn.net/yao_hou/article/details/108015209)

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
